package app

import lwjglutils.OGLBuffers

object GridFactory {

    fun generateGrid(m: Int, n: Int): OGLBuffers {
        val vertexBuffer = FloatArray(2 * m * n)

        var index = 0
        for (j in 0 until n) {
            for (i in 0 until m) {
                vertexBuffer[index++] = i / (m - 1).toFloat()
                vertexBuffer[index++] = j / (n - 1).toFloat()
            }
        }

        index = 0
        val indexBuffer = IntArray(2 * (m + 1) * (n - 1))
        for (j in 0 until n - 1) {
            for (i in 0 until m + 1) {
                if (j % 2 == 0) {
                    if (i == m) {
                        indexBuffer[index++] = (i - 1) + (j + 1) * m
                        indexBuffer[index++] = (i - 1) + (j + 1) * m
                    } else {
                        indexBuffer[index++] = i + j * m
                        indexBuffer[index++] = i + (j + 1) * m
                    }
                } else {
                    if (i == m) {
                        indexBuffer[index++] = (m) - i + (j + 1) * m
                        indexBuffer[index++] = (m) - i + (j + 1) * m
                    } else {
                        indexBuffer[index++] = (m - 1) - i + (j + 1) * m
                        indexBuffer[index++] = j * m + (m - 1) - i
                    }
                }
            }
        }

        val attribs = arrayOf(OGLBuffers.Attrib("inPosition", 2))
        return OGLBuffers(vertexBuffer, attribs, indexBuffer)
    }
}
