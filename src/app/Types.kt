package app

object Types {

    fun decrement(value: Int, min: Int = 0): Int {
        return if (value.minus(1) < min) min else value.minus(1);
    }

    fun increment(value: Int, max: Int = 0): Int {
        return if (value.plus(1) > max) max else value.plus(1);
    }

}