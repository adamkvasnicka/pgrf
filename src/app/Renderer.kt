package app

import lwjglutils.*
import org.lwjgl.BufferUtils
import org.lwjgl.glfw.GLFWCursorPosCallback
import org.lwjgl.glfw.GLFWKeyCallback
import org.lwjgl.glfw.GLFWMouseButtonCallback
import org.lwjgl.glfw.GLFWScrollCallback
import org.lwjgl.glfw.GLFWWindowSizeCallback
import org.lwjgl.opengl.GL30
import transforms.Camera
import transforms.Mat4PerspRH
import transforms.Vec3D

import java.io.IOException

import org.lwjgl.glfw.GLFW.*
import org.lwjgl.opengl.GL11C
import org.lwjgl.opengl.GL20C.*
import transforms.Mat4OrthoRH

class Renderer : AbstractRenderer() {
    private var buffers: OGLBuffers? = null

    private var locView: Int = 0
    private var locProjection: Int = 0
    private var locTime: Int = 0
    private var locType: Int = 0
    private var locColorType: Int = 0

    private var locViewWave: Int = 0
    private var locProjectionWave: Int = 0
    private var locTimeWave: Int = 0

    private var locViewSun: Int = 0
    private var locProjectionSun: Int = 0
    private var locPositionSun: Int = 0

    private var locViewLight: Int = 0
    private var locProjectionLight: Int = 0
    private var locTimeLight: Int = 0
    private var locTypeLight: Int = 0
    private var locLightMode: Int = 0
    private var locLightVP: Int = 0
    private var locLightV: Int = 0

    private var shaderProgramLight: Int = 0
    private var shaderProgramViewer: Int = 0
    private var shaderProgramWave: Int = 0
    private var shaderProgramSun: Int = 0

    internal var ox: Double = 0.toDouble()
    internal var oy: Double = 0.toDouble()

    internal var mouseButton1 = false

    private var cam: Camera? = null
    private var camLight: Camera? = null
    private var projectionPers: Mat4PerspRH? = null
    private var projectionOrtho: Mat4OrthoRH? = null

    private var time: Float = 0.toFloat()
    private var shape: Int = 1
    private var colorType: Int = 0
    private var lightMode: Int = 0
    private var cameraType: Int = 0
    private var programType: Int = 0
    private var renderType: Int = GL11C.GL_TRIANGLE_STRIP

    private var renderTarget: OGLRenderTarget? = null
    private var viewer: OGLTexture2D.Viewer? = null
    private var textureMosaic: OGLTexture2D? = null

    override var keyCallback = object : GLFWKeyCallback() {
        override fun invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true)
            if (action == GLFW_PRESS || action == GLFW_REPEAT) {
                println("Key $key pressed")
                when (key) {
                    GLFW_KEY_W -> cam = cam?.forward(1.0)
                    GLFW_KEY_D -> cam = cam?.right(1.0)
                    GLFW_KEY_S -> cam = cam?.backward(1.0)
                    GLFW_KEY_A -> cam = cam?.left(1.0)
                    GLFW_KEY_LEFT_CONTROL -> cam = cam?.down(1.0)
                    GLFW_KEY_LEFT_SHIFT -> cam = cam?.up(1.0)
                    GLFW_KEY_SPACE -> cam = cam?.withFirstPerson(!cam!!.firstPerson)
                    GLFW_KEY_R -> cam = cam?.mulRadius(0.9)
                    GLFW_KEY_F -> cam = cam?.mulRadius(1.1)
                    GLFW_KEY_C -> cameraType = if (cameraType == 0) 1 else 0
                    GLFW_KEY_P -> programType = if (programType == 0) 1 else 0
                    GLFW_KEY_O -> renderType = if (renderType == GL11C.GL_TRIANGLE_STRIP) GL11C.GL_LINE_STRIP else GL11C.GL_TRIANGLE_STRIP
                    GLFW_KEY_UP -> shape = Types.increment(shape, 6)
                    GLFW_KEY_DOWN -> shape = Types.decrement(shape, 1)
                    GLFW_KEY_Q -> colorType = Types.increment(colorType, 5)
                    GLFW_KEY_E -> colorType = Types.decrement(colorType, 0)
                    GLFW_KEY_RIGHT -> lightMode = Types.increment(lightMode, 2)
                    GLFW_KEY_LEFT -> lightMode = Types.decrement(lightMode, 0)
                }
            }
        }
    }

    override var wsCallback = object : GLFWWindowSizeCallback() {
        override fun invoke(window: Long, w: Int, h: Int) {
            if (w > 0 && h > 0 &&
                    (w != width || h != height)) {
                width = w
                height = h
                projectionPers = Mat4PerspRH(Math.PI / 4, height / width.toDouble(), 0.01, 1000.0)
                projectionOrtho = Mat4OrthoRH(1.0, 1.0, 0.01, 1000.0)
                textRenderer?.resize(width, height)
            }
        }
    }

    var mbCallback = object : GLFWMouseButtonCallback() {
        override fun invoke(window: Long, button: Int, action: Int, mods: Int) {
            mouseButton1 = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                mouseButton1 = true
                val xBuffer = BufferUtils.createDoubleBuffer(1)
                val yBuffer = BufferUtils.createDoubleBuffer(1)
                glfwGetCursorPos(window, xBuffer, yBuffer)
                ox = xBuffer.get(0)
                oy = yBuffer.get(0)
            }

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                mouseButton1 = false
                val xBuffer = BufferUtils.createDoubleBuffer(1)
                val yBuffer = BufferUtils.createDoubleBuffer(1)
                glfwGetCursorPos(window, xBuffer, yBuffer)
                val x = xBuffer.get(0)
                val y = yBuffer.get(0)
                cam = cam!!.addAzimuth(Math.PI * (ox - x) / width).addZenith(Math.PI * (oy - y) / width)
                ox = x
                oy = y
            }
        }
    }

    var cpCallback = object : GLFWCursorPosCallback() {
        override fun invoke(window: Long, x: Double, y: Double) {
            if (mouseButton1) {
                cam = cam!!.addAzimuth(Math.PI * (ox - x) / width).addZenith(Math.PI * (oy - y) / width)
                ox = x
                oy = y
            }
        }
    }

    override var scrollCallback = object : GLFWScrollCallback() {
        override fun invoke(window: Long, dx: Double, dy: Double) {
            cam = if (dy < 0)
                cam?.mulRadius(0.9)
            else
                cam?.mulRadius(1.1)
        }
    }

    override fun init() {
        glClearColor(0.5f, 0.5f, 1f, 1f)
        glEnable(GL_DEPTH_TEST)

        shaderProgramViewer = ShaderUtils.loadProgram("/start")

        shaderProgramWave = ShaderUtils.loadProgram("/wave")

        shaderProgramLight = ShaderUtils.loadProgram("/light")

        shaderProgramSun = ShaderUtils.loadProgram("/sun")

        locView = glGetUniformLocation(shaderProgramViewer, "view")
        locProjection = glGetUniformLocation(shaderProgramViewer, "projection")
        locTime = glGetUniformLocation(shaderProgramViewer, "time")
        locType = glGetUniformLocation(shaderProgramViewer, "type")
        locColorType = glGetUniformLocation(shaderProgramViewer, "colorType")
        locLightMode = glGetUniformLocation(shaderProgramViewer, "lightMode")
        locLightVP = glGetUniformLocation(shaderProgramViewer, "lightViewProjection")
        locLightV = glGetUniformLocation(shaderProgramViewer, "lightView")

        locViewWave = glGetUniformLocation(shaderProgramWave, "view")
        locProjectionWave = glGetUniformLocation(shaderProgramWave, "projection")
        locTimeWave = glGetUniformLocation(shaderProgramWave, "time")

        locViewSun = glGetUniformLocation(shaderProgramSun, "view")
        locProjectionSun = glGetUniformLocation(shaderProgramSun, "projection")
        locPositionSun = glGetUniformLocation(shaderProgramSun, "position")

        locViewLight = glGetUniformLocation(shaderProgramLight, "view")
        locProjectionLight = glGetUniformLocation(shaderProgramLight, "projection")
        locTimeLight = glGetUniformLocation(shaderProgramLight, "time")
        locTypeLight = glGetUniformLocation(shaderProgramLight, "type")

        buffers = GridFactory.generateGrid(5, 100)

        renderTarget = OGLRenderTarget(1024, 1024)
        viewer = OGLTexture2D.Viewer()
        try {
            textureMosaic = OGLTexture2D("./textures/earthcloudmap.jpg")
        } catch (e: IOException) {
            e.printStackTrace()
        }

        camLight = Camera()
                .withPosition(Vec3D(0.0, 0.0, 0.0))
                .withAzimuth(5 / 4f * Math.PI)
                .withZenith(-1 / 5f * Math.PI)
                .withFirstPerson(false)
                .withRadius(5.0)
        cam = Camera()
                .withPosition(Vec3D(0.0, 0.0, 0.0))
                .withAzimuth(5 / 4f * Math.PI).withZenith(-1 / 5f * Math.PI)
                .withFirstPerson(false).withRadius(6.0)

        projectionPers = Mat4PerspRH(Math.PI / 3, (LwjglWindow.HEIGHT / LwjglWindow.WIDTH.toFloat()).toDouble(), 1.0, 200.0)
        projectionOrtho = Mat4OrthoRH(10.0, 10.0, 1.0, 200.0)

        textRenderer = OGLTextRenderer(width, height)
    }

    private fun clearColorBuffer() {
        glClearColor(0f, 0f, 0f, 1f)
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
    }

    private fun calculateSunPosition() {
        camLight = camLight?.addAzimuth(0.01)
    }

    private fun displayText() {
        textRenderer?.clear()
        textRenderer?.addStr2D(2, 20, TextFactory.generateControlText())
        textRenderer?.addStr2D(2, 35, TextFactory.generateTextForCameraType(cameraType))
        textRenderer?.addStr2D(2, 50, TextFactory.generateTextForShapeType(shape))
        textRenderer?.addStr2D(2, 65, TextFactory.generateTextForLightType(lightMode))
        textRenderer?.addStr2D(2, 80, TextFactory.generateTextForColorType(colorType))
        textRenderer?.draw()
        GL11C.glEnable(GL11C.GL_DEPTH_TEST)
    }

    override fun display() {
        time = time.plus(0.01f)
        calculateSunPosition()
        if (programType == 0) {
            renderFromLight()
            renderFromViewer()
            renderSun()
        } else
            renderWave()
        glViewport(0, 0, width, height)
        displayText()
    }

    private fun renderSun() {
        glUseProgram(shaderProgramSun)

        glUniformMatrix4fv(locViewSun, false, cam!!.viewMatrix.floatArray())

        glUniformMatrix4fv(locProjectionSun, false, projectionPers!!.floatArray())

        glUniform3f(locPositionSun, camLight!!.eye.x.toFloat(), camLight!!.eye.y.toFloat(), camLight!!.eye.z.toFloat())

        buffers?.draw(renderType, shaderProgramSun)
    }

    private fun renderFromLight() {
        glUseProgram(shaderProgramLight)
        renderTarget?.bind()

        clearColorBuffer()

        glUniformMatrix4fv(locViewLight, false, camLight!!.viewMatrix.floatArray())

        if (cameraType == 0)
            glUniformMatrix4fv(locProjectionLight, false, projectionPers!!.floatArray())
        else
            glUniformMatrix4fv(locProjectionLight, false, projectionOrtho!!.floatArray())

        glUniform1f(locTimeLight, time)

        glUniform1f(locTypeLight, 0f)
        buffers?.draw(renderType, shaderProgramLight)

        glUniform1f(locTypeLight, shape.toFloat())

        buffers?.draw(renderType, shaderProgramLight)
    }

    private fun renderWave() {
        glUseProgram(shaderProgramWave)
        glViewport(0, 0, width, height)

        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0)
        clearColorBuffer()

        glUniformMatrix4fv(locViewWave, false, cam!!.viewMatrix.floatArray())

        glUniformMatrix4fv(locProjectionWave, false, projectionPers!!.floatArray())

        glUniform1f(locTimeWave, time)

        buffers?.draw(renderType, shaderProgramWave)
    }

    private fun renderFromViewer() {
        glUseProgram(shaderProgramViewer)
        glViewport(0, 0, width, height)

        GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0)
        clearColorBuffer()

        glUniformMatrix4fv(locView, false, cam!!.viewMatrix.floatArray())

        if (cameraType == 0)
            glUniformMatrix4fv(locProjection, false, projectionPers!!.floatArray())
        else
            glUniformMatrix4fv(locProjection, false, projectionOrtho!!.floatArray())

        textureMosaic?.bind(shaderProgramViewer, "textureMosaic", 0)

        glUniformMatrix4fv(locLightV, false, camLight!!.viewMatrix.floatArray())

        if (cameraType == 0)
            glUniformMatrix4fv(locLightVP, false, camLight!!.viewMatrix.mul(projectionPers).floatArray())
        else
            glUniformMatrix4fv(locLightVP, false, camLight!!.viewMatrix.mul(projectionOrtho).floatArray())

        renderTarget!!.depthTexture.bind(shaderProgramViewer, "depthTexture", 1)

        glUniform1f(locTime, time)
        glUniform1f(locType, 0f)
        glUniform1f(locColorType, colorType.toFloat())
        glUniform1f(locLightMode, lightMode.toFloat())
        buffers?.draw(renderType, shaderProgramViewer)

        glUniform1f(locTime, time)
        glUniform1f(locColorType, colorType.toFloat())
        glUniform1f(locType, shape.toFloat())
        glUniform1f(locLightMode, lightMode.toFloat())
        buffers?.draw(renderType, shaderProgramViewer)
    }
}

// TODO: Add posibility to change size of grid
