package app

import org.lwjgl.glfw.GLFWCursorPosCallback
import org.lwjgl.glfw.GLFWKeyCallback
import org.lwjgl.glfw.GLFWMouseButtonCallback
import org.lwjgl.glfw.GLFWScrollCallback
import org.lwjgl.glfw.GLFWWindowSizeCallback

import lwjglutils.OGLTextRenderer
import lwjglutils.OGLUtils

import org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE
import org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_1
import org.lwjgl.glfw.GLFW.GLFW_PRESS
import org.lwjgl.glfw.GLFW.GLFW_RELEASE
import org.lwjgl.glfw.GLFW.glfwGetCursorPos
import org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose
import org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT
import org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT
import org.lwjgl.opengl.GL11.glClear
import org.lwjgl.opengl.GL11.glClearColor
import org.lwjgl.opengl.GL11.glViewport

import org.lwjgl.BufferUtils
import kotlin.math.cos
import kotlin.math.sin

abstract class AbstractRenderer {
    private var pass: Int = 0
    protected var width: Int = 0
    protected var height: Int = 0
    protected var textRenderer: OGLTextRenderer? = null

    open var keyCallback: GLFWKeyCallback = object : GLFWKeyCallback() {
        override fun invoke(window: Long, key: Int, scancode: Int, action: Int, mods: Int) {
            if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE)
                glfwSetWindowShouldClose(window, true)
            when (action) {
                GLFW_RELEASE -> println("Key release $key")
                GLFW_PRESS -> println("Key pressed $key")
            }
        }
    }

    open var wsCallback: GLFWWindowSizeCallback = object : GLFWWindowSizeCallback() {
        override fun invoke(window: Long, w: Int, h: Int) {
            if (w > 0 && h > 0) {
                width = w
                height = h
                println("Windows resize to [$w, $h]")
                textRenderer?.resize(width, height)
            }
        }
    }

    open var mouseCallback: GLFWMouseButtonCallback = object : GLFWMouseButtonCallback() {

        override fun invoke(window: Long, button: Int, action: Int, mods: Int) {
            val xBuffer = BufferUtils.createDoubleBuffer(1)
            val yBuffer = BufferUtils.createDoubleBuffer(1)
            glfwGetCursorPos(window, xBuffer, yBuffer)
            val x = xBuffer.get(0)
            val y = yBuffer.get(0)

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                println("Mouse button 1 is pressed at coursor position [$x, $y]")
            }

            if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_RELEASE) {
                println("Mouse button 1 is released at coursor position [$x, $y]")
            }
        }

    }

    open var cursorCallback: GLFWCursorPosCallback = object : GLFWCursorPosCallback() {
        override fun invoke(window: Long, x: Double, y: Double) {}
    }

    open var scrollCallback: GLFWScrollCallback = object : GLFWScrollCallback() {
        override fun invoke(window: Long, dx: Double, dy: Double) {
            println("Mouse wheel velocity $dy")
        }
    }

    open fun init() {
        OGLUtils.printOGLparameters()
        OGLUtils.printLWJLparameters()
        OGLUtils.printJAVAparameters()
        OGLUtils.shaderCheck()
        // Default clear color
        glClearColor(0.0f, 0.0f, 0.0f, 0.0f)
        textRenderer = OGLTextRenderer(width, height)
    }

    open fun display() {
        glViewport(0, 0, width, height)
        val text = this.javaClass.name + ": look at console and try keys, mouse, wheel and window interaction "

        pass = pass.plus(1);
        // Set default clear color
        glClearColor((sin(pass / 100.0) / 2 + 0.5).toFloat(),
                (cos(pass / 200.0) / 2 + 0.5).toFloat(),
                (sin(pass / 300.0) / 2 + 0.5).toFloat(), 0.0f)
        // Clear BUFFERS
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)

        // Draw text to window
        textRenderer?.clear()
        textRenderer?.addStr2D(3, 20, text)
        textRenderer?.addStr2D(3, 50, "pass $pass")
        textRenderer?.addStr2D(width - 90, height - 3, " (c) PGRF UHK")
        textRenderer?.draw()
    }

}