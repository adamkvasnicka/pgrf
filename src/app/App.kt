package app


object App {

    @JvmStatic
    fun main(args: Array<String>) {
        LwjglWindow(Renderer())
    }

}