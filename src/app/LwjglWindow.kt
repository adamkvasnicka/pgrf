package app

import org.lwjgl.glfw.*
import org.lwjgl.opengl.*
import org.lwjgl.system.*

import org.lwjgl.glfw.Callbacks.*
import org.lwjgl.glfw.GLFW.*
import org.lwjgl.system.MemoryStack.*
import org.lwjgl.system.MemoryUtil.*

class LwjglWindow(private val renderer: Renderer, width: Int = WIDTH, height: Int = HEIGHT, debug: Boolean = false) {
    private var window: Long = 0

    init {
        DEBUG = debug
        WIDTH = width
        HEIGHT = height
        run()
    }

    private fun run() {
        init()
        loop()
        glfwFreeCallbacks(window)
        glfwDestroyWindow(window)
        glfwTerminate()
        glfwSetErrorCallback(null)?.free()
    }

    private fun init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set()

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        check(glfwInit()) { "Unable to initialize GLFW" }

        // Configure GLFW
        glfwDefaultWindowHints() // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE) // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE) // the window will be resizable

        var text = renderer.javaClass.name
        text = text.substring(0, text.lastIndexOf('.'))
        // Create the window
        window = glfwCreateWindow(WIDTH, HEIGHT, text, NULL, NULL)
        if (window == NULL)
            throw RuntimeException("Failed to create the GLFW window")

        // Setup a key callback. It will be called every time a key is pressed, repeated or released.
        glfwSetKeyCallback(window, renderer.keyCallback)
        glfwSetWindowSizeCallback(window, renderer.wsCallback)
        glfwSetMouseButtonCallback(window, renderer.mbCallback)
        glfwSetCursorPosCallback(window, renderer.cpCallback)
        glfwSetScrollCallback(window, renderer.scrollCallback)

        // Get the thread stack and push a new frame
        stackPush().use { stack ->
            val pWidth = stack.mallocInt(1) // int*
            val pHeight = stack.mallocInt(1) // int*

            // Get the window size passed to glfwCreateWindow
            glfwGetWindowSize(window, pWidth, pHeight)

            // Get the resolution of the primary monitor
            val vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor())

            // Center the window
            glfwSetWindowPos(
                    window,
                    (vidmode!!.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            )
        } // the stack frame is popped automatically

        // Make the OpenGL context current
        glfwMakeContextCurrent(window)
        // Enable v-sync
        glfwSwapInterval(1)

        // Make the window visible
        glfwShowWindow(window)
    }

    private fun loop() {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities()

        if (DEBUG)
            GLUtil.setupDebugMessageCallback()

        renderer.init()

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!glfwWindowShouldClose(window)) {

            renderer.display()

            glfwSwapBuffers(window) // swap the color buffers

            // Poll for window events. The key callback above will only be
            // invoked during this call.
            glfwPollEvents()
        }
    }

    companion object {

        var WIDTH = 600
        var HEIGHT = 400

        private var DEBUG = false

        init {
            if (DEBUG) {
                System.setProperty("org.lwjgl.util.Debug", "true")
                System.setProperty("org.lwjgl.util.NoChecks", "false")
                System.setProperty("org.lwjgl.util.DebugLoader", "true")
                System.setProperty("org.lwjgl.util.DebugAllocator", "true")
                System.setProperty("org.lwjgl.util.DebugStack", "true")
                Configuration.DEBUG_MEMORY_ALLOCATOR.set(true)
            }
        }
    }

}