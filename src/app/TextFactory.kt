package app

object TextFactory {

    fun generateTextForShapeType(shape: Int): String {
        return "Shape type $shape"
    }

    fun generateTextForLightType(light: Int): String {
        return "Light type $light"
    }

    fun generateTextForCameraType(camera: Int): String {
        return "Camera type $camera"
    }

    fun generateTextForColorType(color: Int): String {
        return "Color type $color"
    }

    fun generateControlText(): String {
        return "Control"
    }

}