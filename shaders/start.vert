#version 150
in vec2 inPosition;

uniform mat4 view;
uniform mat4 projection;
uniform float time;
uniform mat4 lightViewProjection;
uniform mat4 lightView;
uniform float colorType;
uniform float type;

out vec3 vertColor;
out vec3 normal;
out vec3 lightDirection;
out vec3 viewDirection;
out vec4 depthTextureCoord;
out vec2 texCoord;
out float lightIntensity;
out float lightDistance;
out vec3 reflectorDirection;
out vec3 spotDirection;

vec3 getSphere(vec2 vec){
    float az = vec.x * 3.14;
    float ze = vec.y * 3.14 / 2.0;
    float r = 1.0;

    float x = r * cos(az) * cos(ze);
    float y = 2.0 * r * sin(az) * cos(ze);
    float z = 0.5 * r * sin(ze);
    return vec3(x, y, z);
}

vec3 getPlane(vec2 vec){
    return vec3(vec * 4, -1);
}

vec3 getElephantHead(vec2 vec) {
    float z = 1.0;
    float r = sqrt(vec.x * vec.x + vec.y * vec.y +  z * z);
    float s = atan(vec.x, vec.y);
    float t = acos(z / r);

    r = 3.0 + cos(4.0 * s);

    float x = r * sin(t) * cos(s);
    float y = r * sin(t) * sin(s);
    z = r * cos(t);

    return vec3(x, y, z);
}

vec3 getHedhedog(vec2 vec) {
    float z = 1.0;
    float r = sqrt(vec.x * vec.x + vec.y * vec.y +  z * z);
    float s = atan(vec.x, vec.y);
    float t = acos(z / r);

    r = 1 + 0.4 * sin(10 * s) * sin(10 * t);

    float x = r * sin(t) * cos(s);
    float y = r * sin(t) * sin(s);
    z = r * cos(t);

    return vec3(x, y, z);
}

vec3 getDrop(vec2 vec) {
    float s = 3.14159 * vec.x;
    float t = 3.14159 * vec.y;

    float theta = s;
    float r = 1 + cos(t);
    float z = 2 - t;

    float x = r * cos(theta);
    float y = r * sin(theta);

    return vec3(x, y, z);
}

vec3 getJuicer(vec2 vec) {
    float s = 3.14159 * vec.x;
    float t = 3.14159 * vec.y;

    float theta = s;
    float r = t;
    float z = cos(t);

    float x = r * cos(theta);
    float y = r * sin(theta);

    return vec3(x, y, z);
}

vec3 getPyramid(vec2 vec) {
    float finalZ = 1.0 - abs(vec.x + vec.y) - abs(vec.y - vec.x);
    return vec3(vec.x, vec.y, finalZ);
}

vec3 getNormalForTransformation(vec2 vec, float type) {
    vec3 u;
    vec3 v;
    vec2 uVec2 = vec2(0.001, 0);
    vec2 vVec2 = vec2(0, 0.001);
    if(type == 0.0){
        u = getPlane(vec + uVec2) - getPlane(vec - uVec2);
        v = getPlane(vec + vVec2) - getPlane(vec - vVec2);
    } else if (type == 1.0) {
        u = getSphere(vec + uVec2) - getSphere(vec - uVec2);
        v = getSphere(vec + vVec2) - getSphere(vec - vVec2);
    } else if (type == 2.0) {
        u = getElephantHead(vec + uVec2) - getElephantHead(vec - uVec2);
        v = getElephantHead(vec + vVec2) - getElephantHead(vec - vVec2);
    } else if (type == 3.0) {
        u = getDrop(vec + uVec2) - getDrop(vec - uVec2);
        v = getDrop(vec + vVec2) - getDrop(vec - vVec2);
    } else if (type == 4.0) {
        u = getPyramid(vec + uVec2) - getPyramid(vec - uVec2);
        v = getPyramid(vec + vVec2) - getPyramid(vec - vVec2);
    } else if (type == 5.0) {
        u = getJuicer(vec + uVec2) - getJuicer(vec - uVec2);
        v = getJuicer(vec + vVec2) - getJuicer(vec - vVec2);
    } else if (type == 6.0) {
        u = getHedhedog(vec + uVec2) - getHedhedog(vec - uVec2);
        v = getHedhedog(vec + vVec2) - getHedhedog(vec - vVec2);
    } else {
        u = getSphere(vec + uVec2) - getSphere(vec - uVec2);
        v = getSphere(vec + vVec2) - getSphere(vec - vVec2);
    }
    return cross(u,v);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 pos3;
    vec4 pos4;

    if(type == 0.0){
        pos3 = getPlane(pos);
    } else if (type == 1.0) {
        pos3 = getSphere(pos);
    } else if (type == 2.0) {
        pos3 = getElephantHead(pos);
    } else if (type == 3.0) {
        pos3 = getDrop(pos);
    } else if (type == 4.0) {
        pos3 = getPyramid(pos);
    } else if (type == 5.0) {
        pos3 = getJuicer(pos);
    } else if (type == 6.0) {
        pos3 = getHedhedog(pos);
    } else {
        pos3 = getSphere(pos);
    }
    pos4 = vec4(pos3, 1.0);

    normal = mat3(view) * getNormalForTransformation(pos, type);

    if (colorType == 4.0) {
        normal = mat3(view) * getNormalForTransformation(pos, type);
        normal = normalize(normal);
        normal = inverse(transpose(mat3(view))) * normal;
    }

    gl_Position = projection * view * pos4;

    vec3 lightPosition = vec3(0, 0, 0);

    lightDirection = lightPosition - (view * pos4).xyz;
    viewDirection = -(view * pos4).xyz;
    reflectorDirection = lightPosition - (lightView * pos4).xyz;
    spotDirection = -(lightViewProjection * pos4).xyz;

    lightDistance = length(viewDirection);
    lightIntensity = dot(normalize(lightDirection), normalize(normal));

    depthTextureCoord = lightViewProjection * pos4;
    depthTextureCoord.xyz = depthTextureCoord.xyz / depthTextureCoord.w;
    depthTextureCoord.xyz = (depthTextureCoord.xyz + 1) / 2;

    texCoord = inPosition;
    vertColor = pos4.xyz;
} 
