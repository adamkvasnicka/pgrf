#version 150
in vec2 inPosition;

uniform mat4 view;
uniform mat4 projection;
uniform float time;

uniform float type;

vec3 getSphere(vec2 vec){
    float az = vec.x * 3.14;
    float ze = vec.y * 3.14 / 2.0;
    float r = 1.0;

    float x = r * cos(az) * cos(ze);
    float y = 2.0 * r * sin(az) * cos(ze);
    float z = 0.5 * r * sin(ze);
    return vec3(x, y, z);
}

vec3 getPlane(vec2 vec){
    return vec3(vec * 4, -1);
}

vec3 getElephantHead(vec2 vec) {
    float z = 1.0;
    float r = sqrt(vec.x * vec.x + vec.y * vec.y +  z * z);
    float s = atan(vec.x, vec.y);
    float t = acos(z / r);

    r = 3.0 + cos(4.0 * s);

    float x = r * sin(t) * cos(s);
    float y = r * sin(t) * sin(s);
    z = r * cos(t);

    return vec3(x, y, z);
}

vec3 getHedhedog(vec2 vec) {
    float z = 1.0;
    float r = sqrt(vec.x * vec.x + vec.y * vec.y +  z * z);
    float s = atan(vec.x, vec.y);
    float t = acos(z / r);

    r = 1 + 0.4 * sin(10 * s) * sin(10 * t);

    float x = r * sin(t) * cos(s);
    float y = r * sin(t) * sin(s);
    z = r * cos(t);

    return vec3(x, y, z);
}

vec3 getDrop(vec2 vec) {
    float s = 3.14159 * vec.x;
    float t = 3.14159 * vec.y;

    float theta = s;
    float r = 1 + cos(t);
    float z = 2 - t;

    float x = r * cos(theta);
    float y = r * sin(theta);

    return vec3(x, y, z);
}

vec3 getJuicer(vec2 vec) {
    float s = 3.14159 * vec.x;
    float t = 3.14159 * vec.y;

    float theta = s;
    float r = t;
    float z = cos(t);

    float x = r * cos(theta);
    float y = r * sin(theta);

    return vec3(x, y, z);
}

vec3 getPyramid(vec2 vec) {
    float finalZ = 1.0 - abs(vec.x + vec.y) - abs(vec.y - vec.x);
    return vec3(vec.x, vec.y, finalZ);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 pos3;

    if(type == 0.0){
        pos3 = getPlane(pos);
    } else if (type == 1.0) { // Per vertex
        pos3 = getSphere(pos);
    } else if (type == 2.0) {
        pos3 = getElephantHead(pos);
    } else if (type == 3.0) {
        pos3 = getDrop(pos);
    } else if (type == 4.0) {
        pos3 = getPyramid(pos);
    } else if (type == 5.0) {
        pos3 = getJuicer(pos);
    } else if (type == 6.0) {
        pos3 = getHedhedog(pos);
    } else {
        pos3 = getSphere(pos);
    }

    gl_Position = projection * view * vec4(pos3, 1.0);
}