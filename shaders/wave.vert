#version 150
in vec2 inPosition;

uniform mat4 view;
uniform mat4 projection;
uniform float time;

out vec3 vertColor;

uniform float type;

// HSL to RGB code inspired by
// http://www.niwa.nu/2013/05/math-behind-colorspace-conversions-rgb-hsl/
float hue2rgb(float t1, float t2, float hue)
{
    if (hue < 0) hue += 1;
    if (hue > 1) hue -= 1;
    if ((6 * hue) < 1) return t1 + (t2 - t1) * 6 * hue;
    if ((2 * hue) < 1) return t2;
    if ((3 * hue) < 2) return t1 + (t2 - t1) * (2 / 3 - hue) * 6;
    return t1;
}

vec3 hsl2rgb(float height)
{
    // Basic setting
    float luminance = 0.5;
    float saturation = 1;

    float r, g, b, temp1, temp2;

    if(luminance < 0.5) {
        temp1 = luminance * (1 + saturation);
    } else if (luminance > 0.5) {
        temp1 = luminance + saturation - luminance * saturation;
    }

    temp2 = 2 * luminance - temp1;

    r = hue2rgb(temp1, temp2, height + (1 / 3));
    g = hue2rgb(temp1, temp2, height);
    b = hue2rgb(temp1, temp2, height - (1 / 3));

    return vec3(r, g, b);
}

float waveFunction(in vec2 pos, float time) {
    float offset = time;
    return 0.2 * sin((pos.x + offset) * 5.0) * cos((pos.y + offset) * 5.0);
}

void main() {
    vec2 pos = inPosition.xy;
    vec3 pos3;
    vec4 pos4;

    float resultZ = waveFunction(pos, time);

    pos3 = vec3(inPosition.x, inPosition.y, resultZ);
    pos4 = vec4(pos3, 1.0);

    vertColor = hsl2rgb(resultZ);

    gl_Position = projection * view * pos4;
}

