#version 150
in vec2 inPosition;

uniform mat4 view;
uniform mat4 projection;

uniform vec3 position;

vec3 getSun(vec2 vec){
    float az = vec.x * 3.14;
    float ze = vec.y * 3.14;
    float r = 1.0;

    float x = r * cos(az) * cos(ze);
    float y = r * sin(az) * cos(ze);
    float z = r * sin(ze);
    return vec3(x, y, z);
}

void main() {
    vec2 pos = inPosition * 2 - 1;
    vec3 pos3;
    
    vec4 pos4;

    pos3 = getSun(pos * 2);

    pos4 = vec4(pos3 + vec3(position.x, position.y + 1, position.z + 1), 1.0);

    gl_Position = projection * view * pos4;
}

