#version 150
out vec4 outColor;

in vec3 vertColor;
in vec3 normal;
in vec3 lightDirection;
in vec3 viewDirection;
in vec4 depthTextureCoord;
in vec2 texCoord;
in float lightDistance;
in float lightIntensity;
in vec3 reflectorDirection;
in vec3 spotDirection;

uniform float colorType;
uniform float lightMode;
uniform float type;
uniform sampler2D textureMosaic;
uniform sampler2D depthTexture;

void main() {
    vec4 ambient = vec4(vec3(0.3), 1);
    float NdotL = max(0, dot(normalize(normal), normalize(lightDirection)));
    vec4 diffuse = vec4(NdotL*vec3(0.4), 1);
    vec3 halfVector = normalize(normalize(lightDirection) + normalize(viewDirection));
    float NdotH = dot(normalize(normal), halfVector);
    vec4 specular = vec4(pow(NdotH, 16) * vec3(1), 1);

    vec4 textureColor = texture(textureMosaic, texCoord);

    bool shadow = texture(depthTexture, depthTextureCoord.xy).r <  depthTextureCoord.z - 0.001;

    float att = 1.0 / (0.4 + 0.004 * lightDistance + 0.005 * lightDistance * lightDistance);

    float spotCutOff = 0.7;
    float spotEffect = max(dot(normalize(spotDirection), normalize(-normalize(reflectorDirection))), 0);
    float blend = clamp((spotEffect-spotCutOff) / (1 - spotCutOff), 0.0, 1.0);

    if (colorType == 1.0) {
        outColor = normalize(vec4(normalize(normal), 1));
    } else if (colorType == 2.0) {
        outColor = depthTextureCoord;
    } else if (colorType == 3.0) {
        float inten = dot(normalize(lightDirection), normalize(normal));

        vec4 color;
        if (inten > 0.95) color = vec4(1.0, 0.5, 0.5, 1.0);
        else if (inten > 0.80) color = vec4(0.6, 0.3, 0.3, 1.0);
        else if (inten > 0.50) color = vec4(0.0, 0.0, 0.3, 1.0);
        else if (inten > 0.25) color = vec4(0.4, 0.2, 0.2, 1.0);
        else color = vec4(0.2, 0.1, 0.1, 1.0);

        outColor = color;
    } else if (colorType == 4.0) {
        vec4 color;
        if (lightIntensity > 0.95)  color = vec4(1.0, 0.5, 0.5, 1.0);
        else if (lightIntensity > 0.80)  color = vec4(0.6, 0.3, 0.3, 1.0);
        else if (lightIntensity > 0.50)  color = vec4(0.0, 0.0, 0.3, 1.0);
        else if (lightIntensity > 0.25)  color = vec4(0.4, 0.2, 0.2, 1.0);
        else color = vec4(0.2, 0.1, 0.1, 1.0);

        outColor = color;
    } else if (colorType == 5.0) {
        outColor = vec4(normalize(depthTextureCoord.xyz), 1.0);
    } else {
        outColor = textureColor;
    }
    if (lightMode == 2.0) {
        if (spotEffect > spotCutOff) {
            if (shadow) {
                outColor = ambient * outColor;
            }
            else {
                outColor = mix(ambient, ambient + att * (diffuse + specular), blend) * outColor;
            }
        } else {
            outColor = ambient * outColor;
        }
    } else if (lightMode == 1.0) {
        if (shadow) {
            outColor = ambient * outColor;
        } else {
            outColor = ambient + att * (diffuse + specular) * outColor;
        }
    } else if (lightMode == 0.0) {
        if (shadow) {
            outColor = ambient * outColor;
        } else {
            vec4 finalColor = diffuse + ambient + specular;
            outColor = finalColor * outColor;
        }
    }
} 
